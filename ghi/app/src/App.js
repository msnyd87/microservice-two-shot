import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import HatsList from "./HatsList";
import HatForm from "./HatForm";
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import React from 'react';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          {/* <HatForm /> */}
          <Route path="/" element={<MainPage />} />
           {/* <Route
            path="hats/list"
            element={<HatsList hats={props.hats} />}
            /> */}
          <Route path="shoes" element={<ShoesList shoes={props.shoes} />} />
          <Route path="shoes/new" element={<ShoeForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
