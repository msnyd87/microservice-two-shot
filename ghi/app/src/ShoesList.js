// import React, { useState, useEffect } from 'react';

// function ShoesList() {
//     const [shoes, setShoes] = useState([]);

//     const fetchData = async () => {
//         const url = 'http://localhost:8080/api/shoes/';

//         const response = await fetch(url)

//         if (response.ok) {
//             const data = await response.json();
//             setShoes(data.shoes)
//         }
//     }

//     useEffect(() => {
//         fetchData();
//     }, []);

// }

// export default ShoeList;


import { useState } from 'react';


function ShoesList(props) {
    function refreshShoes() {
        window.location.reload(false);
    }

    async function deleteShoe(id) {
        const shoeURL = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(shoeURL, fetchConfig);
        if (response.ok) {
            refreshShoes();
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Manufacturer</th>
                <th>Model Name</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Bin</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes && props.shoes.map(shoe => {
                return (
                        <tr key={shoe.id}>
                            <td>{ shoe.manufacturer }</td>
                            <td>{ shoe.model_name }</td>
                            <td>{ shoe.color }</td>
                            <td><img src={ shoe.picture_url } height="100" width="100" /></td>
                            <td>{ shoe.bin }</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => {deleteShoe(shoe.id)}}>Delete</button>
                            </td>
                        </tr>
                    );
                    })}
            </tbody>
        </table>
    );
}

export default ShoesList;
