# Generated by Django 4.0.3 on 2023-06-02 03:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0003_alter_shoe_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='shoe',
            options={},
        ),
    ]
