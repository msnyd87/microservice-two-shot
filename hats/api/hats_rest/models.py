from django.db import models
from django.urls import reverse


# Create your models here.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True, null=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)


class Hats(models.Model):
    name = models.CharField(max_length=40)
    fabric = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    picture_url = models.URLField(null=True)
    locations = models.ForeignKey(
        LocationVO, related_name="hats", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

    # class Meta:
    #     ordering = ["name"]

    def get_api_url(self):
        return reverse("api_show_hats", kwargs={"id": self.id})
