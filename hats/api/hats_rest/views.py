from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hats, LocationVO


# Create your views here.
class HatsEncoder(ModelEncoder):
    model = Hats
    properties = ["name", "fabric", "color", "picture_url", "id"]

    def get_extra_data(self, o):
        return {
            "locations": {
                "closet_name": o.locations.closet_name,
                "href": o.locations.href,
            }
        }


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
        ]



# Create your views here.
class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "fabric",
        "color",
        "picture_url",
        "id",
        ]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "name",
        "color",
        "picture_url",
    ]
    encoders = {
        "location": LocationVODetailEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(locations=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse({"hats": hats}, encoder=HatsListEncoder)
    else:
        content = json.loads(request.body)
        try:
            location_href = content["locations"]
            location = LocationVO.objects.get(id=location_href)
            content["locations"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        hats = Hats.objects.create(**content)
        return JsonResponse(hats, encoder=HatDetailEncoder, safe=False)


@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        hat = Hats.objects.get(id=id)
        return JsonResponse(hat, encoder=HatsDetailEncoder, safe=False)
    else:
        count, _ = Hats.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
